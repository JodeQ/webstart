
**Saas** (**S**yntactically **A**wesome **S**tyle**s**heets) est un language de génération de feuille de style (**CSS**) initialement développé par Hampton Catlin et Nathalie Weizenbaum.

Lorsque l'on parle de **Sass** il y a deux choses :
 - L'outil **Saas** préprocesseur qui permet de générer du code css
 - Le language de script **SassScript** qui est le language de script qui peut être utilisé dans du code **Sass**  

Deux syntaxes existent. La syntaxe originale, nommée « syntaxe indentée », est proche de Haml. La nouvelle syntaxe se nomme SCSS. Elle a un formalisme proche de CSS. 

Le code css permet de faire énormément de chose dans les pages web mais avec le temps ces feuilles de styles sont devenues de plus en plus longues et complexes. **Sass** ajoute des fonctionnalités qui n'existent pas en **CSS** comme :
 - Les variables
 - l'imbrication (nesting)
 - l'incorporation (mixins)
 - l'héritage (inheritance)
 - ...

Travailler avec **Sass** va nous ajouter une étape de compilation pour obtenir le css lisible par les navigateurs et pour cela nous devons installer des outils sur nos postes de travail.
