/* Lorsque l'on déplace un élément dragabble, on stocke son attribut id dans l'évènement de transfert */
function drag(ev) {
  ev.stopPropagation();
  ev.dataTransfer.setData("text", ev.target.id);
  ev.dropEffect = "move";
}

/* Désactive le comortement par défaut de l'évènement qui empêche le drop par défaut */
function allowDrop(ev) {
  ev.preventDefault();
}

/* lorsque l'on dépose un élément on récupère son id dans "data" puis on ajoute en enfant l'élément dans la cible du dépôt */
function drop(ev, el) {
  ev.preventDefault();
  var data = ev.dataTransfer.getData("text");
  el.appendChild(document.getElementById(data));
}

var nbTasks = 4;

function createTask(ev, el) {
  if ((ev.which == 13 || ev.keyCode == 13) && ev.srcElement.value) {
    nbTasks = nbTasks + 1;
    /* task = document.createElement('div');
    task.setAttribute("id", "tache" + nbTasks);
    task.setAttribute("class", "w3-card");
    task.setAttribute("draggable", "true");
    task.setAttribute("ondragstart", "drag(event)");
    task.textContent = ev.srcElement.value;
    el.parentElement.appendChild(task);
    */

    $(el.parentElement).append('<div id="tache' + nbTasks + '" class="w3-card task" draggable="true" ondragstart="drag(event)">' + ev.srcElement.value + '</div>');
    $('#tache' + nbTasks).dblclick(function(ev) {
      loadEditor(ev.target)
    });
    //console.log(task);
    ev.srcElement.value = '';
  }
}

function loadEditor(el) {
  // Récupération du contenu de la tâche choisie
  $('#title').val($(el).text());
    
  // Affichage de la modale
  $('#editor').show();
}

$(document).ready(function() {
  $('.task').dblclick(function(ev) {
    loadEditor(ev.target)
  }) 
});