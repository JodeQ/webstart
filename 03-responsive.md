# Responsive web design (RWD)

Le responsive web design ou site web adaptatif est l'utilisation avancée du html et du css pour automatiquement redimensionné, caché, rétrécir ou agrandir un site web pour qu'il soit bien présenté sur les différents supports (Ordinateurs, tablettes ou téléphones)

## La définition du viewport

Pour créer des pages responsive, ajoutez la balise meta suivante
```
 <meta name="viewport" content="width=device-width, initial-scale=1.0"> 
```

Cela configurera la vue de votre page en disant à votre navigateur comment la page peut être redimmensionnée.

Sans la balise meta 

![](https://www.w3schools.com/css/img_viewport1.png)

Avec la balise meta

![](https://www.w3schools.com/css/img_viewport2.png)

## Images responsives

Les images responsives sont des images qui se redimensionnent pour rentrer parfaitement dans la taille du navigateur

Pour rendre une image responsive la propriété css `width` est fixé en pourcentage de la page

```
 <img src="https://www.w3schools.com/html/img_girl.jpg" style="width:100%;"> 
```
Dans cet exemple la largeur de l'image fera 100% de son élément parent.

Si la page est très large alors l'image pourrait être plus large que sa taille original et fera donc apparaître des pixels, une meilleure solution dans la plupart des cas sera d'utiliser la propriété css `max-width`

```
<img src="https://www.w3schools.com/html/img_girl.jpg" style="max-width:100%;height:auto;"> 
```

Maintenant l'image se réduira si la page est plus petite que la largeur de l'image, mais l'image ne dépassera jamais sa largeur d'origine.

## Changer d'image selon la résolution

La balise `picture` permet de définir différentes images selon les dimensions du navigateur

```
 <picture>
  <source srcset="img_smallflower.jpg" media="(max-width: 600px)">
  <source srcset="img_flowers.jpg" media="(max-width: 1500px)">
  <source srcset="flowers.jpg">
  <img src="img_smallflower.jpg" alt="Flowers">
</picture> 
```

## Texte responsive

Il existe plusieurs unités de dimmensions en html. Pour du texte responsive l'unité `vw` "viewport width" est celle qu'il faut utiliser

```
 <h1 style="font-size:10vw">Mon texte responsive</h1> 
 ```

Le viewport est la dimension du navigateur. 1vw = 1% de la largeur du viewport. Si le viewport fait 50cm, 1vw = 0,5cm.

## Les media queries

En complément des redimensonnement des textes et des images il est possible de faire appel aux media queries.

Avec les media queries vous pouvez définir différent styles pour les différentes tailles de navigateur.

| Bureau | tablette | téléphone |
|----|-----|-----|
|![](https://www.w3schools.com/css/rwd_desktop.png)|![](https://www.w3schools.com/css/rwd_tablet.png)|![](https://www.w3schools.com/css/rwd_phone.png)

La syntaxe media queries se fait dans les fichiers de style css avec le mot clé `@media` qui définit les `breakpoints` 

La configuration de base de bootstrap est celle-ci
```
/* 
plus petits périphériques (téléphones en mode portrait, moins de 576px)
Pas de media query puisque c'est le format par défaut de bootstrap 
*/

/* Petits périphériques (téléphones en mode paysage, 576px et +) */
@media (min-width: 576px) { ... }

/* Périphériques moyens (tablettes, 768px et +) */
@media (min-width: 768px) { ... }

/* Périphériques larges (Bureaux, 992px et +) */
@media (min-width: 992px) { ... }

/* Périphériques extra large (Bureaux larges, 1200px et +) */
@media (min-width: 1200px) { ... }
```

