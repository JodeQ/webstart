// récupération du contexte
const canvas = document.querySelector('#board');
const ctx = canvas.getContext("2d");
console.log(ctx.canvas);

// déinition des couleurs de dessin
ctx.strokeStyle = "black";
ctx.fillStyle = "red";

// Dessin d'un rectangle
//            x    y   largeur  hauteur
ctx.fillRect(300, 200, 50, 50);
ctx.strokeRect(300, 200, 50, 50);

// Dessin d'un cercle
ctx.beginPath();
//       x    y   rayon début fin
ctx.arc(325, 170, 25, 0, Math.PI * 2, false);
ctx.fill();
ctx.stroke();

// récupération de la hauteur et largeur du canvas dans w et h
const { width: w, height: h} = canvas;

ctx.fillStyle = "#555";

let x, y, radius;

for (let i = 0; i < 550; i++) {
    x = Math.random() * w;
    y = Math.random() * h;
    radius = Math.random() * 3;

    // Dessiner les étoiles
    ctx.beginPath();
    ctx.arc(x, y, radius, 0, Math.PI * 2, false);
    ctx.fill();
}


// le texte dans canvas
ctx.font = "20pt courier";

const center = w / 2;
ctx.textAlign = "center";
ctx.fillStyle = "white";

ctx.fillText("La tête dans les étoiles", center, 30);


// Les images dans canvas
const img = new Image();
img.src = "res/images/rick.png";
// Les images doivent préalablement être chargées pour pouvoir être dessinées
img.addEventListener("load", draw, false);

function draw() {
    ctx.drawImage(img, 100, 100);
}