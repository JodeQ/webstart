// nombre de Rick à cliquer
let clickers = 50;
// Date de départ du jeu
let startTime = Date.now();

// Positionner les éléments dans le DOM
function sync(dom, pos) {
    dom.style.left = `${pos.x}px`;
    dom.style.top = `${pos.y}px`;
}

function addClicker () {
    const pos = {
        x: Math.random() * 500,
        y: Math.random() * 300
    };
    // Création de l'image
    const img = new Image();
    img.src = "res/images/rick.png";
    img.style.position = "absolute";
    img.addEventListener("click", removeClicker, false);
    // Ajout de l'image dans le DOM
    document.querySelector('#board').appendChild(img);
    // Déplace l'image
    sync(img, pos);
}

function removeClicker(e) {
    e.target.parentNode.removeChild(e.target);
    clickers--;     // clickers = clikers - 1
    checkGameOver();
}

function checkGameOver() {
    // Drawing Things
    document.querySelector("#remain").innerHTML = clickers;
    if (clickers === 0) {
        const taken = Math.round((Date.now() - startTime) / 1000);
        alert(`De-rick-é en ${taken} secondes !`);
    }
}

// Ajoutez tous les Ricks !
for (let i = 0; i < clickers; i++) {
    addClicker();
}
