//const gameType = "awesome";
//alert(`Let's make ${ gameType } games !`);

// génère un nombre aléatoire entre 1 et 20
const myGuess = Math.floor(Math.random() * 20) + 1;
let guesses = 0;
let guess;

// Tant que la valeur proposée par l'utilisateur n'est pas la bonne
while(guess !== myGuess) {
    // Afficher une popup pour récupérer la valeur de l'utilisateur
    guess = parseInt(prompt("A quel nombre, entre 1 et 20, je pense ?"), 10);
    
    // on augmente de 1 le nombre de tentative
    guesses++;    // guesses = guesses + 1

    if (guess < myGuess) {
        alert("C'est plus grand");
    } else if (guess > myGuess) {
        alert("C'est plus petit");
    }
}

alert(`Bien joué ! Vous avez trouvé en ${ guesses } essai(s) !`);
