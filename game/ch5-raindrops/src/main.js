const canvas = document.querySelector("#board");
const ctx = canvas.getContext("2d");
const { width: w, height: h } = canvas;

// Couleur de remplissage par défaut
ctx.fillStyle = "#000";

// Niveau de transparence
ctx.globalAlpha = 0.02;

// Position initiale de la balle
var balleX = w / 2;    // centre des x
var balleY = h / 2;    // centre des y

// Position de la souris
var mouseX = 0;
var mouseY = 0;

// Suivi du déplacement
function mouseMove(e) {
    var rect = e.target.getBoundingClientRect();
    mouseX = e.clientX - rect.left;
    mouseY = e.clientY - rect.top;
    console.log(mouseX);
}

canvas.onmousemove = mouseMove;

function boucle() {
    requestAnimationFrame(boucle);

    // Logique du jeu
    // suavegarde des valeurs du contexte
    ctx.save();

    // Effacement de l'écran
    ctx.fillRect(0, 0, w, h);

    // Changement des couleurs
    ctx.fillStyle = "#fff";
    ctx.globalAlpha = 1;

    // Disques aléatoire en position et rayon
    const x = Math.random() * w;
    const y = Math.random() * h;
    const radius = Math.random() * 20; // Rayon maximum de 20px

    ctx.beginPath();
    ctx.arc(x, y, radius, 0, Math.PI * 2);
    ctx.fill();

    // Calcul de déplacement
    var dx = (mouseX - balleX) * 0.0125;
    var dy = (mouseY - balleY) * 0.0125;
    
    // Déplacement de la balle
    balleX += dx;
    balleY += dy;

    // Dessin de la balle
    ctx.fillStyle = "#f00";
    ctx.beginPath();
    ctx.arc(balleX, balleY, 30, 0, Math.PI * 2);
    ctx.fill();

    // Restauration du contexte
    ctx.restore();
}
// Lancement de l'animation
requestAnimationFrame(boucle);
