
// les boucles d'animation
// L'ancienne méthode : setInterval();
// premier paramètre : fonction anonyme
// deuxième paramètre temps en milliseconde : 1000/60 = 60 tours par seconde
/*
setInterval( () => {
    // ici le code a animer

    // si fin du jeu appel de clearInterval
    clearInterval()
}, 1000 / 60);
*/
// on sort de la boucle avec clearInterval()

const canvas = document.querySelector("#board");
const ctx = canvas.getContext("2d");
const { width: w, height: h } = canvas;

ctx.fillStyle = "#000";
ctx.strokeStyle = "#fff";
ctx.font = "40pt monospace";


const start = Date.now();
const timer = setInterval(() => {
    // effacer l'écran
    ctx.fillRect(0, 0, w / 2, h);

    // Affichage du temps
    ctx.strokeText(Date.now() - start, 20, 80);
    ctx.strokeText('Interval', 0, h - 20);

    // Vérification de la fin
    if (Math.random() < 0.01) {
        ctx.strokeText("Fin !", 20, 180);
        clearInterval(timer);
    }
}, 1000 / 60);



// Deuxième boucle : setTimeout
function boucle() {
    ctx.fillRect(w / 2, 0, w, h);
    ctx.strokeText('Timeout', w / 2, h - 20);
    ctx.strokeText(Date.now() - start, w / 2, 80);
    if (Math.random() < 0.01) {
        ctx.strokeText("Fin !", w / 2 + 20, 180);
    } else {
        // boucle
        setTimeout(boucle, 1000 / 60);
    }
}

// démarrage de la boucle
boucle();



// Méthode depuis HTML5 : requestAnimationFrame
function anim(t) {
    requestAnimationFrame(anim);

    // Logique du jeu

}

// démarrage
requestAnimationFrame(anim);


