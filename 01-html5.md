# HTML5 (HyperText Markup Language 5)

Au début des années 1990, au CERN à Genève, Tim Berners-Lee et Robert Cailliau définissent pour l'internet public naissant un langage de balisage d'hypertexte conçu pour représenter des pages web.

La notion d'hypertexte permet de lier différents documents entre eux via des hyperliens. C'est ainsi qu'en consultant une page web via un navigateur nous pouvons changer de page en cliquant sur les (hyper)liens.

HTML est donc un document structuré permettant de présenter des informations à travers un navigateur.

La version actuelle de HTML est la 5. Sa principale évolution est d'ajouter des balises sémantiques afin de permettre aux applications de mieux comprendre le contenu d'une page web.

## Structure de base

```
<!doctype html>
<html lang="fr">
<head>
  <meta charset="utf-8">
  <title>Titre de la page</title>
  <link rel="stylesheet" href="style.css">
  <script src="script.js"></script>
</head>
<body>
  ...
  <!-- commentaire HTML -->
  <!-- Le corps de la page -->
  ...
</body>
</html>
```

Un document html contient un ensemble de balises qui encadrent le contenu, par exemple la balise ouvrante `<p>` se fermera à la prochaine balise fermante `</p>`.

Une balise peut contenir des attributs, par exemple la balise `html` peut contenir l'attribut `lang` et ainsi définir la langue de la page. La syntaxe d'un attribut se fera ainsi `<balise attribut1="valeur1" attribut2="valeur2">`.

**En résumé** : 
- Une page web est composée de **balises**.
- Une **balise** s'écrit avec un chevron ouvrant `<` au début et un chevron fermant `>` à la fin.
- Une **balise** s'ouvre avec une balise ouvrante `<p>` puis se ferme avec la balise fermante `</p>`, tous ce qui est encadré par ce couple est dit _contenu_ dans la balise.
- Certaines **balise** sont dites _orpheline_ car elle ne peuvent pas contenir d'élément, par exemple la balise `<img>` qui sert juste à indiquer d'insérer une image à cet endroit du document.
- Une **balise** peut contenir des **attributs**.
- Les **attributs** s'écrivent à l'intérieur de la **balise** ouvrante, entre le libellé de la balise et le chevron fermant `>`, de la forme `attribut="valeur"`, exemple : `<img src="ma_photo.jpg">`. 

L'ensemble des balises et attributs possibles est disponible sur le site du [W3C (World Wide Web Consortium)](https://www.w3schools.com/tags/default.asp)

### Le doctype
Le doctype permet de préciser au navigateur le type de document et ainsi lui permettre de choisir le meilleur rendyu possible.

### L'élément racine `html`
L'élément racine `html` contient l'ensemble du document qui est décomposé en deux parties `head` pour l'en-tête et `body` pour le corps de la page.

### Les éléments `head` et `body`

la balise d'en-tête du document `head` sert à ajouter des méta-informations ou méta-données, c'est-à-dire des données qui ne sont pas reprénsentées directement à l'écran mais qui sont tout de même liées, telles que le titre, les mots-clés, la description générale de la page et aussi des liens vers des ressources externes telles que les feuilles de style ou des fichiers javascripts qui permettront de modifier la mise en forme de la page.

La balise du corps `body` contient toute la partie visible de la page qui sera rendu par le navigateur en suivant les règles définis dans les feuilles de style et modifier par le code javascript.

### L'encodage des caractères

Un ordinateur travaille uniquement en binaire, des 0 et des 1 ou dans un language plus marketing avec des "digits" d'où le nom de digital malheureusement utilisé à la place de numérique. Un fichier texte tel qu'un document HTML utilise donc un encodage de caractère (charset) qui permet de passer d'une représentation binaire (suite de 0 et de 1) à une représentation textuelle lisible par nous. Le charset UTF8 est le plus riche et permet d'afficher le plus de langues aujourd'hui, il est le choix le plus judicieux pour nos pages web.

La balise `<meta charset="utf8">` permet donc de dire au navigateur que le document actuel est encodé en UTF8.

### Le chargement de ressources externes

La balise `link` permet de dire au navigateur qu'il doit récupérer un autre fichier pour rendre pleinement cette page web. La plupart du temps il s'agira des feuilles de style via l'attribut `rel="stylesheet"` mais cela peut être aussi d'autres types de relations telles que l'auteur, des icônes, le document suivant ou précédent s'il s'agit d'une série de document, ...

### Le javascript

Pour utiliser le javascript dans votre page vous devrez faire appel à la balise `script`, par exemple `<script src="script.js"></script>`

### Les autres éléments HTML

Avec une structure de base votre page peut déjà être affichée dans un navigateur et nous allons tester cela ensemble.

La liste des éléments à tester est assez conséquentes, en voici un extrait

* Éléments de section `section`, `article`, `header`, `footer`, `nav` et `aside`
* Élément `datalist`
* Éléments `details` & `summary`
* Éléments `figure` & `figcaption`
* Éléments média `audio` et `video` complétés par `source`
* Élément `canvas`
* Éléments `meter`, `output` et `progress`
* Élément `time` et `mark`

## Un mot sur les navigateurs

Il existe beaucoup de navigateur comme :
 * Firefox
 * Chrome
 * Safari
 * Edge
 * Opera

## Les familles de balises

### Les titres

Il existe 6 niveaux de titres html, ce sont les balises h1 à h6. h1 le plus grand niveau de titre et h6 le plus petit.
```
<h1>Mon titre de niveau 1</h1>
<h2>Mon titre de niveau 2</h2>
<h3>Mon titre de niveau 3</h3>
<h4>Mon titre de niveau 4</h4>
<h5>Mon titre de niveau 5</h5>
<h6>Mon titre de niveau 6</h6>
```
qui donnerai

***

# Mon titre de niveau 1
## Mon titre de niveau 2
### Mon titre de niveau 3
#### Mon titre de niveau 4
##### Mon titre de niveau 5
###### Mon titre de niveau 6

***

### Les paragraphes

La balise `<p>` est la balise qui permet d'écrire des paragraphes.

```
<p>Mon paragraphe</p>
```

### Les liens

La balise `<a>` est la balise des liens. c'est avec l'attribut `href` que l'on précise la destination du lien. En cliquant sur le contenu entre la balise ouvrante ou la balise fermante vous accéderez au lien.

```
<a href="https://www.qwant.com">Rechercher sur Qwant</a>
```

***
[Rechercher sur Qwant](https://www.qwant.com)
***

### Les images

les images html sont définies par la balise `img`. les principaux attributs sont la source `src`, le texte alternatif `alt`, la largeur `width` et la hauteur `height`.

```
 <img src="img/w3schools.jpg" alt="W3Schools.com" width="104" height="142"> 
 ```

***
![W3Schools.com](img/w3schools.jpg)
***

### Les boutons

Les boutons sont crées avec la balise `button`

```
 <button>Cliquer moi</button> 
```

### Les listes

Il existe deux types de listes, les listes ordonnées `ol` (ordered list) qui mettent un numéro devant chaque élément de la liste et les listes non ordonnées `ul` (unordered list).
Pour ces deux listes chaque élément devra être contenu dans une balise `li`

```
 <ul>
  <li>Café</li>
  <li>Thé</li>
  <li>Lait</li>
</ul> 
```

***
* Café
* Thé
* Lait
***

### Les lignes horizontales

La balise orpheline `hr` permet d'insérer une ligne horizontale dans le document

```
<p>mon paragraphe</p>
<hr>
<p>mon autre paragraphe</p>
```

mon paragraphe
***
mon autre paragraphe

### Les sauts de lignes

Faire un saut de ligne simple en html se fait avec la balise orpheline `br`

```
<p>je veux un saut de ligne ici<br>ce texte sera à la ligne</p>
```

***
je veux un saut de ligne ici<br>ce texte sera à la ligne
***


### Conserver le format source

En html les espaces ou les sauts de lignes ne sont pas forcément respectés, par exemple ces 2 paragraphes sont affichés de la même manière
```
<p>Ce paragraphe
contient des
sauts
de
lignes
</p>

<p>
Ce paragraphe
       contient
  aussi
des      espaces
   additionnels
</p>
```

***
Ce paragraphe contient des sauts de lignes

Ce paragraphe contient aussi des espaces additionnels
***

Pour éviter ce comportement il faut utiliser la balise de texte préformatté `pre`

```
<pre>
Je     respecte
    la mise     en
  forme
du code    source
</pre>
```

***
```
Je     respecte
    la mise     en
  forme
du code    source
```
***
