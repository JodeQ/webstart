// Lorsque le document est entièrement chargé
$(document).ready(
  function () {
    // Lorsque l'on clique sur le bouton
    $('#button').click(
      function () {
        // Récupération de la valeur de la tâche
        var toAdd = $('input[name=ListItem]').val();
        // Si la valeur n'est pas vide
        if ( toAdd != '' ) {
          // Ajout d'un <li> dans le <ol>
          $('ol').append('<li>' + toAdd + '</li>');
          // Changement de la valeur dans l'input pour la chaîne de caractère vide : ''
          // (vidage du champ input)
          $('input[name=ListItem]').val('');
        }
      });

    // Lorsque l'on "relâche" une touche dans le champ input
    $("input[name=ListItem]").keyup(function (event) {
      if (event.keyCode == 13) {
        $("#button").click();
      }
    });

    // Lorsque l'on doubleclique sur un élément <li> dans la page
    $(document).on('dblclick', 'li', function () {
      $(this).toggleClass('strike');
    });

    // Lorsque que l'on met le focus sur le champ input
    $('input').focus(function () {
      $(this).val('');
    });

    // JQueryUI pour renuméroter les éléments dans <ol>
    $('ol').sortable();

    // Désactivation de la gestion du comportament par défaut du formulaire
    // (empêche la soumission du formulaire)
    $('form').submit(function(e){ e.preventDefault(); });

  }
);